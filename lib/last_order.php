<?php
namespace Test\GetOrder;


use Bitrix\Main\{Localization\Loc, Loader};
use Bitrix\Sale\Order;

class WriteLastOrder
{

    /**
     * @param $value
     * @param $words
     * @param bool $show
     * @return string
     */
    //Функция склонения
    function num_word($value, $words, $show = true)
    {
        $num = $value % 100;
        if ($num > 19) {
            $num = $num % 10;
        }

        $out = ($show) ?  $value . ' ' : '';
        switch ($num) {
            case 1:  $out .= $words[0]; break;
            case 2:
            case 3:
            case 4:  $out .= $words[1]; break;
            default: $out .= $words[2]; break;
        }

        return $out;
    }

    /**
     * @return string
     */
    //Функция-агент для записи
    public function write()
    {

        Loc::loadMessages(__FILE__);

        //Получим последние заказы
        $arOrder = self::getLastOrder();

        //обяъвляем переменную, в которую будем записывать сумму заказа
        $summ = floatval(0);

        //Если заказов нет, то выходим
        if(empty($arOrder))
            return '\Test\GetOrder\WriteLastOrder::write();';

        //перебираем заказы
        foreach ($arOrder as $order)
        {
            //складываем сумму
            $summ += floatval($order['PRICE']);
        }

        //"форматируем" полученную сумму
        $summ = number_format($summ, 2, '.', ' ');

        //получаем склонение
        $countText = self::num_word(count($arOrder), [
            Loc::getMessage('ONE_NAME_ORDER_PHR'),
            Loc::getMessage('SECOND_NAME_ORDER_PHR'),
            Loc::getMessage('MANY_NAME_ORDER_PHR'),
        ]);

        //очистим переменную
        unset($arOrder);

        //формиурем текст
        $text = date('d.m.Y H:i:s').' '.$countText.' '.$summ;

        //производим запись
        $fopen = fopen($_SERVER['DOCUMENT_ROOT'].'/orders.txt', 'w');
        fwrite($fopen, $text);
        fclose($fopen);

        return '\Test\GetOrder\WriteLastOrder::write();';
    }

    /**
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    //Метод для получения заказов
    public function getLastOrder()
    {
        if(!Loader::includeModule('sale'))
            return false;

        //получаем время в формате TimeStamp
        $dateEndTS = time();
        $dateBeginTS = $dateEndTS-(60*60);

        //Приводим к формату
        $dateBegin = date('d.m.Y H:i:s', $dateBeginTS);
        $dateEnd = date('d.m.Y H:i:s', $dateEndTS);

        //Получаем заказы
        $arOrder = Order::getList([
            'filter' => [
                '>=DATE_INSERT' => $dateBegin,
                '<=DATE_INSERT' => $dateEnd,
            ],
            'select' => ['PRICE']
        ])->fetchAll();

        return $arOrder;
    }

}
