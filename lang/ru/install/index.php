<?php
$MESS['TEST_GETORDER_NAME']                         = 'Запись последних заказов';
$MESS['TEST_GETORDER_DESCRIPTION']                  = 'Модуль для записи последних заказов';
$MESS['TEST_GETORDER_PARTNER_NAME']                 = 'zheev';
$MESS['TEST_GETORDER_PARTNER_URI']                  = 'https://zheev.ru';
$MESS['TEST_GETORDER_INSTALL_ERROR_VERSION']        = 'Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.';
$MESS['TEST_GETORDER_INSTALL_ERROR_AGENT']          = 'Ошибка! Агент не установлен';
