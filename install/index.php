<?php

use Bitrix\Main\{ModuleManager, Localization\Loc, Config\Option};

Loc::loadMessages(__FILE__);

class test_getorder extends CModule
{

    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            $arModuleVersion = [];

            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID           = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME         = Loc::getMessage("TEST_GETORDER_NAME");
            $this->MODULE_DESCRIPTION  = Loc::getMessage("TEST_GETORDER_DESCRIPTION");
            $this->PARTNER_NAME        = Loc::getMessage("TEST_GETORDER_PARTNER_NAME");
            $this->PARTNER_URI         = Loc::getMessage("TEST_GETORDER_PARTNER_URI");
        }
    }

    public function DoUninstall()
    {
        //удаляем агенты модуля
        CAgent::RemoveModuleAgents($this->MODULE_ID);

        //убираем регисрацию модуля
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function DoInstall()
    {

        global $APPLICATION;

        //проверим версию главного ядра
        if(
            CheckVersion(
                ModuleManager::getVersion("main"),
                "14.00.00"
            )
        )
        {

            //добавляем агент
            $idNewAgent = CAgent::AddAgent(
                '\Test\GetOrder\WriteLastOrder::write();',
                $this->MODULE_ID,
                "N",
                60*60,
                "",
                "Y",
                date("d.m.Y G:i:s")
            );

            //проверяем, создан ли агент
            if(!intval($idNewAgent)) {
                $APPLICATION->ThrowException(
                    Loc::getMessage("TEST_GETORDER_INSTALL_ERROR_AGENT")
                );
            }
            //региструрем модуль
            ModuleManager::registerModule($this->MODULE_ID);

        }else{
            $APPLICATION->ThrowException(
                Loc::getMessage("TEST_GETORDER_INSTALL_ERROR_VERSION")
            );
        }

    }

}